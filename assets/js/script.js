$(document).ready(function() {

	//select elements for hoverdir, apply the script to them
	$(function() {
			$('.grid > div').each( function() { $(this).hoverdir(); } );
	});

	//make the home link orange
	$(window).load(function() {
		$('#home_id').css("color", "#ff5800");
		//Gallery overlays are invisible BEFORE Startup, makes visible on-load
		$('.gallery_overlay').css("opacity","1")

	});

	//burger menu show screenfader and popout menu
	$('#mobile_menu').on('click', function() {
		$('.screencover').addClass('show');
		$('.popout_menu').removeClass('slide_hide');
		$('.popout_menu').addClass('slide_show');

	});

	//hide functions below.....................

	//screenfader hide by clicking on it
	$('.screencover').on('click', function() {
		$(this).removeClass('show');
		$(this).addClass('hide');

		$('.popout_menu').removeClass('slide_show');
		$('.popout_menu').addClass('slide_hide');
	});

	//screenfader hide by clicking on X button
	$('#close_menu').on('click', function() {
		$('.screencover').removeClass('show');
		$('.screencover').addClass('hide');

		$('.popout_menu').removeClass('slide_show');
		$('.popout_menu').addClass('slide_hide');
	});


	//screenfader hide by clicking on anchor links
	$('ul li a').on('click', function() {
		$('.screencover').removeClass('show');
		$('.screencover').addClass('hide');

		$('.popout_menu').removeClass('slide_show');
		$('.popout_menu').addClass('slide_hide');
	});

	// smoothscrolling function...........
	$(function() {
		$('a[href*="#"]:not([href="#"])').click(function() {
			if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
				if (target.length) {
					$('html, body').animate({
						scrollTop: target.offset().top
					}, 800);
					return false;
				}
			}
		});
	});

	// text fading function at mobile/tablet size..........
	var smallheadlines = $('h3[class^="smallcontent-"]').hide(),
		i = 0;

	(function cycle() {

		smallheadlines.eq(i).fadeIn(800)
			.delay(5000)
			.fadeOut(800, cycle);

		i = ++i % smallheadlines.length;

	})();

	// text fading function at max screensize...........
	var largeheadlines = $('h3[class^="largecontent-"]').hide(),
		x = 0;

	(function cycle() {
		largeheadlines.eq(x).fadeIn(800)
			.delay(5000)
			.fadeOut(800, cycle);

		x = ++x % largeheadlines.length;

	})();

	// masonry / Isotope.............................................

	$('.grid').isotope({
		// options
		itemSelector: '.grid-item',
		layoutMode: 'fitRows'
	});

	// init Isotope
	var $grid = $('.grid').isotope({
	  // options
	});
	// UI filter items on button click
	$('.filter-button-group').on( 'click', 'button', function() {
	  var filterValue = $(this).attr('data-filter');
	  $grid.isotope({ filter: filterValue });
	});

	// change is-checked class on buttons
	$('.button-group').each( function( i, buttonGroup ) {
	  var $buttonGroup = $( buttonGroup );
	  $buttonGroup.on( 'click', 'button', function() {
	    $buttonGroup.find('.is-checked').removeClass('is-checked');
	    $( this ).addClass('is-checked');
	  });
	});


	// gallery close..................

	$('#gallery_close_button, .gallery_fader_close').on('click', function() {
		$('.gallery_fader').removeClass('fader_show');
	});

	// gallery set active image..................

	$('#gal1').on('click', function() {
		// var $fotoramaDiv = $('.fotorama').fotorama({allowfullscreen: true});
		var $fotoramaDiv = $('.fotorama').fotorama();
		var fotorama = $fotoramaDiv.data('fotorama');
		// fotorama.requestFullScreen();
		fotorama.show(0);

		$('.gallery_fader').addClass('fader_show');
	});


	$('#gal2').on('click', function() {

		var $fotoramaDiv = $('.fotorama').fotorama();
		var fotorama = $fotoramaDiv.data('fotorama');
		fotorama.show(1);
		$('.gallery_fader').addClass('fader_show');
	});

	$('#gal3').on('click', function() {
		// 1. Initialize fotorama manually.
		var $fotoramaDiv = $('.fotorama').fotorama();
		// 2. Get the API object.
		var fotorama = $fotoramaDiv.data('fotorama');
		fotorama.show(2);
		$('.gallery_fader').addClass('fader_show');
	});

	$('#gal4').on('click', function() {
		// 1. Initialize fotorama manually.
		var $fotoramaDiv = $('.fotorama').fotorama();
		// 2. Get the API object.
		var fotorama = $fotoramaDiv.data('fotorama');
		fotorama.show(3);
		$('.gallery_fader').addClass('fader_show');
	});

	$('#gal5').on('click', function() {
		// 1. Initialize fotorama manually.
		var $fotoramaDiv = $('.fotorama').fotorama();
		// 2. Get the API object.
		var fotorama = $fotoramaDiv.data('fotorama');
		fotorama.show(4);
		$('.gallery_fader').addClass('fader_show');
	});

	$('#gal6').on('click', function() {
		// 1. Initialize fotorama manually.
		var $fotoramaDiv = $('.fotorama').fotorama();
		// 2. Get the API object.
		var fotorama = $fotoramaDiv.data('fotorama');
		fotorama.show(5);
		$('.gallery_fader').addClass('fader_show');
	});

	$('#gal7').on('click', function() {
		// 1. Initialize fotorama manually.
		var $fotoramaDiv = $('.fotorama').fotorama();
		// 2. Get the API object.
		var fotorama = $fotoramaDiv.data('fotorama');
		fotorama.show(6);
		$('.gallery_fader').addClass('fader_show');
	});

	$('#gal8').on('click', function() {
		// 1. Initialize fotorama manually.
		var $fotoramaDiv = $('.fotorama').fotorama();
		// 2. Get the API object.
		var fotorama = $fotoramaDiv.data('fotorama');
		fotorama.show(7);
		$('.gallery_fader').addClass('fader_show');
	});

	$('#gal9').on('click', function() {
		// 1. Initialize fotorama manually.
		var $fotoramaDiv = $('.fotorama').fotorama();
		// 2. Get the API object.
		var fotorama = $fotoramaDiv.data('fotorama');
		fotorama.show(8);
		$('.gallery_fader').addClass('fader_show');
	});

	$('#gal10').on('click', function() {
		// 1. Initialize fotorama manually.
		var $fotoramaDiv = $('.fotorama').fotorama();
		// 2. Get the API object.
		var fotorama = $fotoramaDiv.data('fotorama');
		fotorama.show(9);
		$('.gallery_fader').addClass('fader_show');
	});

	$('#gal11').on('click', function() {
		// 1. Initialize fotorama manually.
		var $fotoramaDiv = $('.fotorama').fotorama();
		// 2. Get the API object.
		var fotorama = $fotoramaDiv.data('fotorama');
		fotorama.show(10);
		$('.gallery_fader').addClass('fader_show');
	});

	$('#gal12').on('click', function() {
		// 1. Initialize fotorama manually.
		var $fotoramaDiv = $('.fotorama').fotorama();
		// 2. Get the API object.
		var fotorama = $fotoramaDiv.data('fotorama');
		fotorama.show(11);
		$('.gallery_fader').addClass('fader_show');
	});

	$('#gal13').on('click', function() {
		// 1. Initialize fotorama manually.
		var $fotoramaDiv = $('.fotorama').fotorama();
		// 2. Get the API object.
		var fotorama = $fotoramaDiv.data('fotorama');
		fotorama.show(12);
		$('.gallery_fader').addClass('fader_show');
	});

	$('#gal14').on('click', function() {
		// 1. Initialize fotorama manually.
		var $fotoramaDiv = $('.fotorama').fotorama();
		// 2. Get the API object.
		var fotorama = $fotoramaDiv.data('fotorama');
		fotorama.show(13);
		$('.gallery_fader').addClass('fader_show');
	});

	$('#gal15').on('click', function() {
		// 1. Initialize fotorama manually.
		var $fotoramaDiv = $('.fotorama').fotorama();
		// 2. Get the API object.
		var fotorama = $fotoramaDiv.data('fotorama');
		fotorama.show(14);
		$('.gallery_fader').addClass('fader_show');
	});

	$('#gal16').on('click', function() {
		// 1. Initialize fotorama manually.
		var $fotoramaDiv = $('.fotorama').fotorama();
		// 2. Get the API object.
		var fotorama = $fotoramaDiv.data('fotorama');
		fotorama.show(15);
		$('.gallery_fader').addClass('fader_show');
	});

	$('#gal17').on('click', function() {
		// 1. Initialize fotorama manually.
		var $fotoramaDiv = $('.fotorama').fotorama();
		// 2. Get the API object.
		var fotorama = $fotoramaDiv.data('fotorama');
		fotorama.show(16);
		$('.gallery_fader').addClass('fader_show');
	});

	$('#gal18').on('click', function() {
		// 1. Initialize fotorama manually.
		var $fotoramaDiv = $('.fotorama').fotorama();
		// 2. Get the API object.
		var fotorama = $fotoramaDiv.data('fotorama');
		fotorama.show(17);
		$('.gallery_fader').addClass('fader_show');
	});

	$('#gal19').on('click', function() {
		// 1. Initialize fotorama manually.
		var $fotoramaDiv = $('.fotorama').fotorama();
		// 2. Get the API object.
		var fotorama = $fotoramaDiv.data('fotorama');
		fotorama.show(18);
		$('.gallery_fader').addClass('fader_show');
	});

	$('#gal20').on('click', function() {
		// 1. Initialize fotorama manually.
		var $fotoramaDiv = $('.fotorama').fotorama();
		// 2. Get the API object.
		var fotorama = $fotoramaDiv.data('fotorama');
		fotorama.show(19);
		$('.gallery_fader').addClass('fader_show');
	});

	$('#gal21').on('click', function() {
		// 1. Initialize fotorama manually.
		var $fotoramaDiv = $('.fotorama').fotorama();
		// 2. Get the API object.
		var fotorama = $fotoramaDiv.data('fotorama');
		fotorama.show(20);
		$('.gallery_fader').addClass('fader_show');
	});

	//hover on portfolio paragraphs
	// $('.folio_wrapper').on('mouseenter', function() {
	// 	$(this).addClass('raise_div');	
	// });

	// $('.folio_wrapper').on('mouseleave', function() {
	// 	$(this).removeClass('raise_div');
		
	// });
	//form validation.............................

	$('#contact_form').validate({

		rules: {
			first_name: {
				required: false,
				minlength: 2
			},

			firstName: {
				required: true,
				minlength: 2
			},

			last_name: {
				required: true,
				minlength: 2

			},

			phone_num: {
				required: true,
				number: true,
				minlength: 6
			},

			email_addy: {
				required: true,
				email: true
			},

			comments: {
				required: true,
				minlength: 5
			}
		},

		messages: {
			first_name: {
				required: "Required",
				minlength: "Must be at least 2 Characters"
			},

			last_name: {
				required: "Required",
				minlength: "Must be at least 2 Characters long"
			},

			phone_num: {
				required: "Phone number required",
				number: "You must enter NUMBERS",
				minlength: "Must be at 6 digits"
			},

			email_addy: {
				required: "Required",
				email: "email address must be valid"
			},
			comments: {
				required: "Required",
				minlength: "Must have more than 2 Characters"
			}
		},


		highlight: function(el, errorClass, validClass) {

			$(el).addClass('animated shake redborder').removeClass('greenColor');

			$(el).one('animationend', function() {
				$(this).removeClass('animated shake redborder');
			});
		}, //end highlight function

		unhighlight: function(el, errorClass, validClass) {

			$(el).removeClass('animated shake redborder').addClass('greenColor');

			$(el).one('animationend', function() {
				$(this).addClass('animated shake redborder');
			});
		}, //end un unhighlight function


		// errorLabelContainer: '#errors'

		errorPlacement: function(error, element) {
			error.insertBefore(element); // <- the default
		}

	});

	//CTA Pop-out......................................

	//makes CTA pop out after 3.5 seconds
	setTimeout(function(){
		// alert('it works');
	  $('#call_to_action').addClass('cta_show');
		}, 3000);

	// makes cta dissapear after 15 seconds
	setTimeout(function(){
		// alert('it works');
	  $('#call_to_action').addClass('cta_hide');
	  $('#call_to_action').removeClass('cta_show');
		}, 10000);

	$(window).scroll(function(){
      if($(window).scrollTop() > 3500){
      	// console.log('shitballs');
      	$('#call_to_action').addClass('cta_hide');
		$('#call_to_action').removeClass('cta_show');
      };
     
   });

	$('#close_cta').on('click', function(){
		$('#call_to_action').removeClass('cta_show');
		$('#call_to_action').addClass('cta_hide');
	});

	$('#call_to_action a').on('click', function(){
		$('#call_to_action').removeClass('cta_show');
		$('#call_to_action').addClass('cta_hide');
	});

	$('#open_cta_button').on('click', function(){
		$('#call_to_action').removeClass('cta_hide');
		$('#call_to_action').addClass('cta_show');
	});

}); //ends the document ready function